# balcony-themes-tumblr

Open source themes for Tumblr.

All themes are available in the [Turmblr theme directory](https://www.tumblr.com/themes/by/balconythemes).

Feel free to use and change any of them for your personal use.

All theme have been started with the [tumblr-boilerplate](https://gitlab.com/hugurp/tumblr-boilerplate).

Open an issue if you have a feedback or if you found a bug you'd like to be fixed.

Peace
